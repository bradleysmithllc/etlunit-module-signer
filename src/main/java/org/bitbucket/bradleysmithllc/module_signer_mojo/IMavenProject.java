package org.bitbucket.bradleysmithllc.module_signer_mojo;

import java.util.Map;

public interface IMavenProject
{
	String get(String name);

	Map<String, String> getProperties();

	String getMavenGroupId();
	String getMavenArtifactId();
	String getMavenVersionNumber();
	String getGitBranch();
	String getGitBranchChecksum();
	boolean isGitBranchClean();
}