package org.bitbucket.bradleysmithllc.module_signer_mojo;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class ModuleVersions
{
	private final ClassLoader loader;

	public ModuleVersions(ClassLoader loader)
	{
		this.loader = loader;
	}

	public ModuleVersions()
	{
		this(null);
	}

	public List<IMavenProject> getAvailableVersions()
	{
		List<IMavenProject> mvlist = new ArrayList<IMavenProject>();

		ClassLoader cl = loader == null ? Thread.currentThread().getContextClassLoader() : loader;

		ServiceLoader<IMavenProject> sl = ServiceLoader.load(IMavenProject.class, cl);

		for (IMavenProject ser : sl)
		{
			mvlist.add(ser);
		}

		return mvlist;
	}
}