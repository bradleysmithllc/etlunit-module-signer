package org.bitbucket.bradleysmithllc.module_signer_mojo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MavenProjectImpl implements IMavenProject
{
	private final Map<String, String> properties = new HashMap<String, String>();

	protected void put(String name, String value)
	{
		properties.put(name, value);
	}

	@Override
	public String get(String name)
	{
		return properties.get(name);
	}

	@Override
	public Map<String, String> getProperties()
	{
		return Collections.unmodifiableMap(properties);
	}

	@Override
	public String getMavenGroupId()
	{
		return properties.get("maven.project.groupId");
	}

	@Override
	public String getMavenArtifactId()
	{
		return properties.get("maven.project.artifactId");
	}

	@Override
	public String getMavenVersionNumber()
	{
		return properties.get("maven.project.version");
	}

	@Override
	public String getGitBranch()
	{
		return properties.get("git.branch");
	}

	@Override
	public String getGitBranchChecksum()
	{
		return properties.get("git.branch.version");
	}

	@Override
	public boolean isGitBranchClean()
	{
		return new Boolean(properties.get("git.branch.clean")).booleanValue();
	}

	@Override
	public String toString()
	{
		return "MavenProjectImpl{"
				+ "maven.project.groupId: '" + getMavenGroupId() + "', "
				+ "maven.project.artifactId: '" + getMavenArtifactId() + "',"
				+ "maven.project.version: '" + getMavenVersionNumber() + "'" +
				'}';
	}
}