package org.bitbucket.bradleysmithllc.module_signer_mojo;

public class ModuleVersion
{
	private String mavenProjectGroupId;
	private String mavenProjectArtifactId;
	private String mavenProjectVersion;

	private String gitBranch;
	private String gitBranchClean;
	private String gitBranchVersion;

	public String getMavenProjectGroupId()
	{
		return mavenProjectGroupId;
	}

	public void setMavenProjectGroupId(String mavenProjectGroupId)
	{
		this.mavenProjectGroupId = mavenProjectGroupId;
	}

	public String getMavenProjectArtifactId()
	{
		return mavenProjectArtifactId;
	}

	public void setMavenProjectArtifactId(String mavenProjectArtifactId)
	{
		this.mavenProjectArtifactId = mavenProjectArtifactId;
	}

	public String getMavenProjectVersion()
	{
		return mavenProjectVersion;
	}

	public void setMavenProjectVersion(String mavenProjectVersion)
	{
		this.mavenProjectVersion = mavenProjectVersion;
	}

	public String getGitBranch()
	{
		return gitBranch;
	}

	public void setGitBranch(String gitBranch)
	{
		this.gitBranch = gitBranch;
	}

	public String getGitBranchClean()
	{
		return gitBranchClean;
	}

	public void setGitBranchClean(String gitBranchClean)
	{
		this.gitBranchClean = gitBranchClean;
	}

	public String getGitBranchVersion()
	{
		return gitBranchVersion;
	}

	public void setGitBranchVersion(String gitBranchVersion)
	{
		this.gitBranchVersion = gitBranchVersion;
	}
}