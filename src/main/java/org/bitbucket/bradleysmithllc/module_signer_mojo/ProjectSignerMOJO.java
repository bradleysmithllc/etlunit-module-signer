package org.bitbucket.bradleysmithllc.module_signer_mojo;

import com.sun.codemodel.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Goal which touches a timestamp file.
 *
 * @goal package-meta-info
 * @phase process-sources
 */
public class ProjectSignerMOJO
		extends AbstractMojo
{
	/**
	 * @parameter expression="${outputDirectory}" default-value="${project.build.directory}/generated-sources/module-signer-source/"
	 * @required
	 */
	private File srcDirectory;

	/**
	 * @parameter expression="${outputDirectory}" default-value="${project.build.directory}/generated-sources/module-signer-resource/META-INF/services"
	 * @required
	 */
	private File rsrcDirectory;

	/**
	 * @parameter expression="${project}" default-value="${project}"
	 */
	protected MavenProject mavenProject;

	/**
	 * Helper class to assist in attaching artifacts to the project instance.
	 * project-helper instance, used to make addition of resources simpler.
	 * @component
	 * @required
	 * @readonly
	 */
	private MavenProjectHelper projectHelper;

	/**
	 * @parameter expression="${etlunitFailNotClean}"
	 */
	protected boolean failOnNotClean;

	public void execute()
			throws MojoExecutionException
	{
		String package_ = clean(mavenProject.getGroupId()) + "." + clean(mavenProject.getArtifactId()) + ".version_" + clean(mavenProject.getVersion());

		try
		{
			String directory = "target/generated-sources/module-signer-resource";
			List includes = Collections.singletonList("**/*");
			List excludes = null;

			projectHelper.addResource(mavenProject, directory, includes, excludes);

			JCodeModel cm = new JCodeModel();
			JDefinedClass dc = cm._class(package_ + "." + "MavenProjectInstance");
			dc._extends(MavenProjectImpl.class);

			JMethod m = dc.constructor(0);
			m.mods().setPublic();

			JBlock block = m.body();

			block.directStatement("// System Properties");
			for (Map.Entry<Object, Object> propertyEntry : System.getProperties().entrySet())
			{
				block.directStatement("put(\"" + propertyEntry.getKey() + "\", \"" + StringEscapeUtils.escapeJava(String.valueOf(propertyEntry.getValue())) + "\");");
			}

			block.directStatement("// Project Properties");
			for (Map.Entry<Object, Object> propertyEntry : mavenProject.getProperties().entrySet())
			{
				block.directStatement("put(\"" + propertyEntry.getKey() + "\", \"" + StringEscapeUtils.escapeJava(String.valueOf(propertyEntry.getValue())) + "\");");
			}

			block.directStatement("// Special Properties");
			Repository repo = getGitRepository();

			Git git = getGit(repo);

			ObjectId branchObjectId = repo.resolve(repo.getFullBranch());
			String branch = repo.getFullBranch();

			block.directStatement("put(\"maven.project.groupId\", \"" + StringEscapeUtils.escapeJava(mavenProject.getGroupId()) + "\");");
			block.directStatement("put(\"maven.project.artifactId\", \"" + StringEscapeUtils.escapeJava(mavenProject.getArtifactId()) + "\");");
			block.directStatement("put(\"maven.project.version\", \"" + StringEscapeUtils.escapeJava(mavenProject.getVersion()) + "\");");
			block.directStatement("put(\"git.branch\", \"" + StringEscapeUtils.escapeJava(branch) + "\");");

			String cleanString = getClean(git);

			if (cleanString == null)
			{
				block.directStatement("put(\"git.branch.clean\", \"true\");");
			}
			else
			{
				block.directStatement("put(\"git.branch.clean\", \"false\");");
				block.directStatement("put(\"git.branch.cleanState\", \"" + cleanString + "\");");
				if (failOnNotClean)
				{
					throw new IllegalStateException("Git project not clean");
				}
			}

			block.directStatement("put(\"git.branch.version\", \"" + StringEscapeUtils.escapeJava(branchObjectId.getName()) + "\");");

			//System.out.println(branchObjectId.getName());

			for (Map.Entry<String, Ref> tag : repo.getTags().entrySet())
			{
				//System.out.println(tag.getKey() + ": " + tag.getValue().getObjectId().getName());

				if (tag.getValue().getObjectId().getName().equals(branchObjectId.getName()))
				{
					block.directStatement("put(\"git.tag.name\", \"" + tag.getKey() + "\");");
				}
			}

			directory = "target/generated-sources/module-signer-source/";

			mavenProject.addCompileSourceRoot(directory);

			// output services file
			FileUtils.write(new File(rsrcDirectory, IMavenProject.class.getCanonicalName()), dc.fullName());

			srcDirectory.mkdirs();
			cm.build(srcDirectory);
		}
		catch(IOException exc)
		{
			throw new MojoExecutionException("Error saving source file", exc);
		}
		catch (JClassAlreadyExistsException e)
		{
			throw new MojoExecutionException("Error saving source file", e);
		}
		catch (GitAPIException e)
		{
			throw new MojoExecutionException("Error reading GIT properties", e);
		}
	}

	public static Git getGit(Repository repo)
	{
		return new Git(repo);
	}

	private Repository getGitRepository() throws IOException
	{
		return getGitRepository(mavenProject.getBasedir());
	}

	public static Repository getGitRepository(File baseDir) throws IOException
	{
		return new RepositoryBuilder().findGitDir(baseDir).build();
	}

	public static String getClean(Git git) throws GitAPIException
	{
		Status repoStatus = git.status().call();

		Set<String> repoStatusAdded = repoStatus.getAdded();
		int added = repoStatusAdded.size();

		Set<String> repoStatusChanged = repoStatus.getChanged();
		int changed = repoStatusChanged.size();

		Set<String> repoStatusModified = repoStatus.getModified();
		int modified = repoStatusModified.size();

		Set<String> repoStatusRemoved = repoStatus.getRemoved();
		int removed = repoStatusRemoved.size();

		Set<String> repoStatusUntracked = repoStatus.getUntracked();
		int untracked = repoStatusUntracked.size();

		if ((added + changed + modified + removed + untracked) == 0)
		{
			return null;
		}
		else
		{
			StringBuilder stb = new StringBuilder();
			stb.append("{added[" + added + "],");
			stb.append("changed[" + changed + "],");
			stb.append("modified[" + modified + "],");
			stb.append("removed[" + removed + "],");
			stb.append("untracked[" + untracked + "]}");

			logGitFiles("added", repoStatusAdded);
			logGitFiles("changed", repoStatusChanged);
			logGitFiles("modified", repoStatusModified);
			logGitFiles("removed", repoStatusRemoved);
			logGitFiles("untracked", repoStatusUntracked);

			return stb.toString();
		}
	}

	private static void logGitFiles(String untracked, Set<String> repoStatusUntracked)
	{
		System.out.println("Git files in status [" + untracked + "]:");

		for (String path : repoStatusUntracked)
		{
			System.out.println("\t" + path);
		}
	}

	private String clean(String version)
	{
		return version.replace('.', '_').replace('-', '_');
	}
}