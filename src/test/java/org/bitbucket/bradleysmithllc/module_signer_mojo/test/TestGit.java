package org.bitbucket.bradleysmithllc.module_signer_mojo.test;

import org.bitbucket.bradleysmithllc.module_signer_mojo.ProjectSignerMOJO;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.bitbucket.bradleysmithllc.module_signer_mojo.ProjectSignerMOJO.*;

public class TestGit
{
	@Test
	public void test() throws IOException, GitAPIException
	{
		Repository repo = getGitRepository(new File("."));
		Git git = ProjectSignerMOJO.getGit(repo);

		System.out.println(ProjectSignerMOJO.getClean(git));
	}
}